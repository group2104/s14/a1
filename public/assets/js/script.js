let todoList = [];
let addItem = document.getElementById('addItem');
let clearItem = document.getElementById('clearItem');


addItem.addEventListener('click', function() {
    let todo = document.getElementById('todo').value; 
    todoList.push(todo);
    console.log(todoList);

    let list = "";

    for(let i = 0; i < todoList.length; i++) {
        list += todoList[i] + "<br/>";
    }

    document.getElementById('list').innerHTML = list;
    document.getElementById('length').innerHTML = todoList.length;
    if (todoList.length > 10 ) {
        console.log("You are awesome! Great job!");
        document.getElementById('status').innerHTML = "You are awesome! Great job!";
    } else if (todoList.length == 10) {
        console.log("Very good! You made it!")
        document.getElementById('status').innerHTML = "Very good! You made it!";
    } else if (todoList.length >= 5) {
        console.log("Please improve your work next You are good!")
        document.getElementById('status').innerHTML = "You are good!";
    } else {
        console.log("Please improve your work next time")
        document.getElementById('status').innerHTML = "Please improve your work next time";
    }
    document.getElementById('lastItem').innerHTML = todoList[todoList.length - 1];
    document.getElementById('firstItem').innerHTML = todoList[0];
});


clearItem.addEventListener('click', function() {
    let todo = document.getElementById('todo').value; 
    todoList.splice(0, todoList.length);
    console.log(todoList);
    document.getElementById("list").innerHTML = "";
    document.getElementById('length').innerHTML = "0";
    document.getElementById('lastItem').innerHTML = "None";
        if (todoList.length == 0) {
            console.log("");
            document.getElementById('status').innerHTML = "";
        }
    document.getElementById('firstItem').innerHTML = "None";
});


